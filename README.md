# Browser extensions
### Connect with me:

[![Email Badge](https://cdn.icon-icons.com/icons2/72/PNG/32/email_14410.png)](mailto:oleksandr.zwick@gmail.com)
[![LinkedIn Badge](https://cdn.icon-icons.com/icons2/99/PNG/32/linkedin_socialnetwork_17441.png)](https://www.linkedin.com/in/oleksandr-tsvik/)

## *References*

- [API Chrome](https://developer.chrome.com/docs/extensions/reference/)
- [API Web Extensions MDN](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions)

- [Style Input Range](https://www.cssportal.com/style-input-range/)
- [Styling Cross-Browser Compatible Range Inputs with CSS](https://css-tricks.com/styling-cross-browser-compatible-range-inputs-css/)
