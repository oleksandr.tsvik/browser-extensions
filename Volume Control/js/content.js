chrome.runtime.sendMessage({ action: "init" });

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  const { action, state } = request;

  switch (action) {
    case "change-volume":
      changeVolume(state.volume);
      break;
  }
});

function changeVolume(volume) {
  const $audios = document.getElementsByTagName("audio");
  const $videos = document.getElementsByTagName("video");

  [...$audios, ...$videos].forEach((elem) => {
    elem.volume = volume;
  });
}
