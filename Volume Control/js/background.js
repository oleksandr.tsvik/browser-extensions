const OBJ_ICON_LOGO = {
  32: "../icons/logo-32.png",
  48: "../icons/logo-48.png",
  64: "../icons/logo-64.png",
  128: "../icons/logo-128.png",
};

const OBJ_ICON_MUTE = {
  32: "../icons/mute-32.png",
  48: "../icons/mute-48.png",
  64: "../icons/mute-64.png",
  128: "../icons/mute-128.png",
};

chrome.tabs.onRemoved.addListener((tabId, removeInfo) => {
  chrome.storage.local.remove(tabId.toString());
});

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  const { action, state } = request;

  switch (action) {
    case "init":
      getCurrentTab().then((tab) => {
        if (!tab) {
          return;
        }

        getStorageVolume(tab.id.toString()).then((volume) => {
          setTimeout(() => {
            changeTabVolume(tab, isNumber(volume) ? volume : null);
          }, 1000);
        });
      });

      break;
    case "get-volume":
      getCurrentTab().then((tab) => {
        if (!tab) {
          return sendResponse(state.initVolume);
        }

        getStorageVolume(tab.id.toString()).then((volume) => {
          sendResponse(isNumber(volume) ? volume : state.initVolume);
        });
      });

      return true;
    case "change-volume":
      getCurrentTab().then((tab) => {
        if (!tab) {
          return;
        }

        chrome.storage.local.set({ [tab.id.toString()]: state.volume });
        changeTabVolume(tab, state.volume);
      });

      break;
  }
});

async function getCurrentTab() {
  let queryOptions = { active: true, lastFocusedWindow: true };
  // `tab` will either be a `tabs.Tab` instance or `undefined`.
  let [tab] = await chrome.tabs.query(queryOptions);

  return tab;
}

async function getStorageVolume(key) {
  return chrome.storage.local.get(key).then((data) => data[key]);
}

async function changeTabVolume(tab, volume) {
  if (!isNumber(volume)) {
    return;
  }

  await chrome.tabs.sendMessage(tab.id, {
    action: "change-volume",
    state: { volume },
  });

  chrome.action.setIcon({
    path: volume === 0 ? OBJ_ICON_MUTE : OBJ_ICON_LOGO,
    tabId: tab.id,
  });
}

function isNumber(value) {
  return typeof value === "number" && !Number.isNaN(value);
}
