const INIT_VOLUME = 100;
const MIN_VOLUME = 0;
const MAX_VOLUME = 100;

const $volumeMin = document.getElementById("volume-min");
const $volumeMax = document.getElementById("volume-max");
const $volumeRange = document.getElementById("volume-range");
const $volumeValue = document.getElementById("volume-value");

$volumeRange.addEventListener("input", function () {
  updateVolume($volumeRange.value);
});

chrome.runtime.sendMessage(
  { action: "get-volume", state: { initVolume: INIT_VOLUME / 100 } },
  (volume) => {
    init(volume * 100, MIN_VOLUME, MAX_VOLUME);
  }
);

function init(percentage, min, max) {
  $volumeRange.min = min;
  $volumeRange.max = max;
  $volumeRange.value = parseInt(percentage);

  $volumeMin.innerHTML = `${min}%`;
  $volumeMax.innerHTML = `${max}%`;

  updateVolume(percentage);
}

function updateVolume(percentage) {
  setVolumeValue(percentage);

  const volume = percentage / 100;
  chrome.runtime.sendMessage({ action: "change-volume", state: { volume } });
}

function setVolumeValue(percentage) {
  $volumeValue.innerHTML = `Volume: ${parseInt(percentage)}%`;
}
